import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'sizeFilter',
})
@Injectable()
export class SizeFilterPipe implements PipeTransform {
    transform(items: any[], value: string): any[] {

        // Json validation
        if (!items) {
            return [];
        }

        // Dropdown validation
        if (!value) {
            return items;
        }

        // Filtering json product list by size selected dropdown value
        return items.filter(singleItem =>
            singleItem.size.includes(value)
        );
    }
}
