import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor (private httpService: HttpClient) { }
  arrProducts: string [];

  // Array with values for dropdown
  arrSize:Array<Object> = [ {value: "", label: "Filter by size" },
                            {value: "XS", label: "XS" },
                            {value: "S", label: "S" },
                            {value: "M", label: "M" },
                            {value: "L", label: "L" },
                            {value: "XL", label: "XL" }];
  sizeValue: string;

  // Load external json file
  ngOnInit () {
    this.httpService.get('../assets/products.json').subscribe(
      data => {
        this.arrProducts = data as string [];	 // FILL THE ARRAY WITH DATA.
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }
}
