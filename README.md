# Products Test

The project runs on Node.js and uses Angular JS as an MV Framework

---

[TOC]

## Requirements
- Node.js


## Installing
To install the Test project, simply navigate to the directory where you keep your code and run the following command:

Clone the repository to your local.

cd test-app && npm install -g @angular/cli && npm install

This will checkout the repository and then install and compile all the required assets.


## Running
Use the command `ng serve --open` to run the application and to watch and compile the Javascript and SCSS files. You can now access the project at [http://localhost:4200](http://localhost:4200).
